import Vue from "vue";
import { User } from "@/definitions/User";
import { defineStore } from "pinia";
import { sendRequest } from "@/request";
import { useUserStore } from "./userStore";
import { Customer } from "@/definitions/Customer";

const state: {
  formState: {
    details?: Partial<User>;
    subscription?: object;
    cardData?: object;
  };
} = {
  formState: {},
};

// A store for user details forms.
export const useDetailsStore = defineStore("userDetails", {
  state: () => state,
  actions: {
    finishSubscriptionForm(subscription: object) {
      this.formState.subscription = subscription;
    },
    finishCreditCardForm(cardData: object) {
      this.formState.cardData = cardData;
    },
    async updateUserDetails(details?: Partial<Customer>, id?: number) {
      const userStore = useUserStore();
      const { status, ...user } = await sendRequest<User>(
        `customer/${id ?? userStore.user?.id ?? this.formState.details?.id}`,
        "PATCH",
        { ...(details ? details : this.formState.details) }
      );

      if (status === 200 && !id) {
        userStore.saveUser(user);
        return true;
      }
    },
    // Checkout after choosing subscription and adding credit card
    async finalize() {
      if (this.formState.cardData && this.formState.subscription) {
        const userStore = useUserStore();
        const { status, ...registeredUser } = await sendRequest<User>(
          `customer/${userStore.user?.id}`,
          "PATCH",
          {
            ...this.formState.details,
            subscription: this.formState.subscription,
          }
        );
        if (status === 200) {
          await Vue.swal({
            icon: "success",
            title: "רכישת המנוי הושלמה בהצלחה",
          });
          userStore.saveUser(registeredUser);

          this.router.replace("home");
        }
      }
    },
  },
});
