import { defineStore } from "pinia";
import { sendRequest } from "@/request";
import { useUserStore } from "./userStore";
import { Meal, MealTypes } from "@/definitions/Meals";
import { noGiveUpFoods, favouriteFoods } from "@/data/foods";

const state = (): {
  meals: Record<string, Meal[]>;
  selectedDate: Date;
  preferences: { noGiveUpFoods: string[]; favouriteFoods: string[] };
  presetMeals: Record<MealTypes, Meal[]>;
} => ({
  meals: {},
  selectedDate: new Date(),
  preferences: { noGiveUpFoods: [], favouriteFoods: [] },
  presetMeals: {
    [MealTypes.Breakfast]: [],
    [MealTypes.Lunch]: [],
    [MealTypes.Dinner]: [],
    [MealTypes.Middle]: [],
  },
});
export const parseMealDate = (date: Date | string) =>
  typeof date !== "string"
    ? date.toISOString().split("T")[0]
    : new Date(date).toISOString().split("T")[0];

export const useMealStore = defineStore("customer_meals", {
  state,
  getters: {
    parsedDate({ selectedDate }) {
      return parseMealDate(selectedDate);
    },
    foodOptions({ preferences }) {
      return Array.from(
        new Set([
          ...preferences.favouriteFoods,
          ...preferences.noGiveUpFoods,
          ...favouriteFoods,
          ...noGiveUpFoods,
        ])
      );
    },
  },
  actions: {
    // Load all generic menus
    async loadPresets(customerId?: number | null) {
      if (this.presetMeals[MealTypes.Breakfast].length === 0) {
        const { meals } = await sendRequest<{
          meals: Record<MealTypes, Meal[]>;
        }>(
          `customer/meals/menus${
            customerId ? "?customerId=" + customerId : ""
          }`,
          "GET"
        );
        this.presetMeals = meals;
        this.$patch({ presetMeals: meals });
      }

      return this.presetMeals;
    },
    async findMealsByDate() {
      const userStore = useUserStore();
      if (userStore.user?.id) {
        const { meals } = await sendRequest<{ meals: Meal[] }>(
          `customer/${userStore.user.id}/meals/${this.parsedDate}`,
          "GET"
        );

        if (meals) {
          this.$patch({ meals: { ...this.meals, [this.parsedDate]: meals } });
        }

        return true;
      }
    },
    async saveMeal(type: MealTypes, form: Partial<Meal>) {
      const userStore = useUserStore();
      if (userStore.user?.id) {
        if (!this.meals?.[this.parsedDate]) {
          await this.findMealsByDate();
        }
        const oldMeal = this.meals?.[this.parsedDate]?.find?.(
          (meal) => meal.meal_type === type
        );
        const { meal } = await sendRequest<{ meal: Meal }>(
          `customer/${userStore.user?.id}/meals/${this.parsedDate}`,
          oldMeal ? "PATCH" : "PUT",
          {
            ...form,
            meal_type: type,
          }
        );
        if (meal) {
          this.$patch({
            meals: {
              ...this.meals,
              [this.parsedDate]: oldMeal
                ? this.meals[this.parsedDate].filter(
                    (meal) => meal.meal_type !== type
                  )
                : this.meals[this.parsedDate].concat(meal),
            },
          });
        }
      }
    },
    // Reload user preferences from the server
    async refreshPreferences() {
      const userStore = useUserStore();
      const response = await sendRequest<typeof this.preferences>(
        `customer/${userStore.user?.id}/preferences`,
        "GET"
      );
      this.$patch({
        preferences: {
          noGiveUpFoods: response.noGiveUpFoods,
          favouriteFoods: response.favouriteFoods,
        },
      });

      const newFavourites = response.favouriteFoods.filter(
        (item) => !favouriteFoods.includes(item)
      );

      const newNoGiveUp = response.noGiveUpFoods.filter(
        (item) => !noGiveUpFoods.includes(item)
      );

      return {
        ...response,
        newFavourites,
        newNoGiveUp,
      };
    },
  },
});
