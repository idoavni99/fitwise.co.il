import { defineStore } from "pinia";
import { sendRequest } from "@/request";
import { Machine } from "@/definitions/Machine";
const state = (): {
  machines: Machine[];
} => ({
  machines: [],
});

export const useMachineStore = defineStore("customer_machines", {
  state,
  actions: {
    async getMachines() {
      if (!this.machines.length) {
        const { machines } = await sendRequest<{
          machines: Machine[];
        }>("programs/machines", "GET");

        this.$patch({ machines });

        return machines;
      }

      return this.machines;
    },
    findMachineById(machine_id?: string) {
      return this.machines.find(
        (machine) => machine.machine_id === parseInt(machine_id ?? "")
      );
    },
  },
});
