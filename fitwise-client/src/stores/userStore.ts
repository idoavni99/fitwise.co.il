import Vue, { computed } from "vue";
import { defineStore } from "pinia";
import { format } from "date-fns";
import { User } from "@/definitions/User";
import { sendRequest } from "@/request";
import { EntityTypes } from "@/definitions/EntityTypes";
import { Customer } from "@/definitions/Customer";

const state: {
  isAuthenticated: boolean;
  user?: Partial<Customer>;
  authData?: Pick<User, "email"> & { password: string };
} = {
  isAuthenticated: false,
  authData: { email: "", password: "" },
  user: {
    entity_type: EntityTypes.Customer,
    has_doc: false,
    calendar_per: false,
    first_name: "נועה",
    last_name: "כהן",
  },
};

export const useUserStore = defineStore("user", {
  state: () => state,
  persist: { storage: sessionStorage, paths: ["user", "isAuthenticated"] },
  getters: {
    firstName: (state) => state.user?.first_name,
    fullName: (state) => state.user?.first_name + " " + state.user?.last_name,
    userBirthDate: (state) =>
      computed(() =>
        format(
          state.user?.birthday ? new Date(state.user.birthday) : new Date(),
          "yyyy-MM-dd"
        )
      ).value,
    isCustomer: (state) => state.user?.entity_type === EntityTypes.Customer,
  },
  actions: {
    async authenticate(email: string, password: string) {
      const { status, ...user } = await sendRequest<User | undefined>(
        "accounts/login",
        "POST",
        {
          email: email,
          password: password,
        }
      );

      if (status === 401) {
        Vue.swal({
          title: "ההתחברות נכשלה",
          text: "שם המשתמש או הסיסמא אינם נכונים",
          icon: "error",
        });
        return;
      }

      this.$patch({ authData: { email, password } });

      if (status === 404 || !user?.id) {
        Vue.swal({
          title: "ההתחברות נכשלה",
          text: "המשתמש אינו קיים.",
          icon: "error",
        });

        return;
      }
      this.$patch({ user, authData: undefined, isAuthenticated: true });
      this.router.push("home");
    },
    saveUser(user: User) {
      this.user = user;
      this.$patch({ user, authData: undefined });
    },
    logout() {
      this.isAuthenticated = false;
      this.user = {};
      this.authData = undefined;
      this.$patch({ isAuthenticated: false, user: {}, authData: undefined });
      this.router.push("/login");
    },
  },
});
