import { defineStore } from "pinia";
import { Customer, CustomerEntry } from "@/definitions/Customer";
import { sendRequest } from "@/request";
import { MealTypes, Menu } from "@/definitions/Meals";

// Save which customer the employee selected, and the data for all customers
const state: {
  selectedCustomerId: number | null;
  customers: Record<number, Customer & { menus: Record<MealTypes, Menu[]> }>;
} = {
  customers: {},
  selectedCustomerId: null,
};

export const useCustomersStore = defineStore("customers", {
  state: () => state,
  // Ensure selected customer persists through refreshes
  persist: { storage: sessionStorage, paths: ["selectedCustomerId"] },
  getters: {},
  actions: {
    async fetchCustomers() {
      const { customers } = await sendRequest<{
        customers: CustomerEntry[];
      }>("employees/customers", "GET");
      this.customers = customers.reduce(
        (customers, customer) => ({ [customer.id]: customer, ...customers }),
        {}
      );
      return customers;
    },
    async fetchCustomerDetails() {
      if (
        this.selectedCustomerId &&
        this.customers[this.selectedCustomerId]?.target
      ) {
        return this.customers[this.selectedCustomerId];
      }

      if (this.selectedCustomerId) {
        const customerDetails = await sendRequest<Customer & CustomerEntry>(
          `customer/${this.selectedCustomerId}`,
          "GET"
        );

        this.customers[this.selectedCustomerId] = {
          ...this.customers[this.selectedCustomerId],
          ...customerDetails,
        };
        this.$patch({ customers: this.customers });

        return customerDetails;
      }
    },
    async fetchCustomerMenus(customerId: number) {
      if (this.customers?.[customerId]?.menus) {
        return this.customers[customerId].menus;
      }
      const { meals } = await sendRequest<{
        meals: Record<MealTypes, Menu[]>;
      }>(`customer/meals/menus/?customerId=${customerId}`, "GET");
      this.customers[customerId].menus = meals;
      this.$patch({ customers: this.customers });

      return meals;
    },
    async addMenu(payload: Omit<Menu, "menu_id">) {
      if (this.selectedCustomerId) {
        const { menu } = await sendRequest<{ menu: Menu }>(
          "employees/customers/meals",
          "PUT",
          payload
        );

        this.customers[this.selectedCustomerId].menus[menu.meal_type].push(
          menu
        );
        this.$patch({ customers: this.customers });
      }
    },
    selectCustomer(id: number) {
      this.$patch({ selectedCustomerId: id });
      this.selectedCustomerId = id;
    },
  },
});
