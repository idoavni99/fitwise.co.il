import { defineStore } from "pinia";
import { sendRequest } from "@/request";
import { useUserStore } from "./userStore";
import { Goal } from "@/definitions/Goal";
const state = (): { goals: Goal[]; userId: number | null } => ({
  goals: [],
  userId: null,
});

export type SaveGoalPayload = {
  endDate?: Date | string | null;
  goalType?: number | null;
  goalSubtype?: string | null;
  comments?: string;
  completed?: boolean;
  goal_id?: number;
};

export const useGoalStore = defineStore("customer_goals", {
  state,
  getters: {
    completedGoals: ({ goals }) => goals.filter((goal) => goal.completed),
    pendingGoals: ({ goals }) =>
      goals
        .filter((goal) => !goal.completed)
        .sort(
          (prev, next) =>
            new Date(prev.end_date).getTime() -
            new Date(next.end_date).getTime()
        ),
  },
  actions: {
    // Initialize store by loading goals for registered user or selected customer
    async initStore(id?: number) {
      const userStore = useUserStore();
      const providedId = id || userStore.user?.id;
      if (providedId) {
        if (!this.goals.length || this.userId !== providedId) {
          const { goals } = await sendRequest<{ goals: Goal[] }>(
            `customer/${id || userStore.user?.id}/goals`,
            "GET"
          );

          if (goals) {
            this.$patch({ goals, userId: providedId });
          }
        }
      }
    },
    async addGoal(form: SaveGoalPayload) {
      if (this.userId) {
        const { goal } = await sendRequest<{ goal: Goal }>(
          `customer/${this.userId}/goals`,
          "PUT",
          {
            ...form,
          }
        );
        if (goal) {
          this.$patch({ goals: [...this.goals, goal] });
        }
      }
    },
    async updateGoal(form: SaveGoalPayload) {
      if (this.userId && form.goal_id) {
        const { message } = await sendRequest<{ message: string }>(
          `customer/${this.userId}/goals`,
          "PATCH",
          {
            ...form,
          }
        );
        if (message === "Goal updated") {
          const goal = this.goals.find((goal) => goal.goal_id === form.goal_id);
          if (goal) {
            const updatedGoal: Goal = {
              ...goal,
              completed: form.completed ?? goal.completed,
              end_date:
                form.endDate instanceof Date
                  ? form.endDate.toISOString()
                  : form.endDate ?? goal.end_date,
              comments: form.comments ?? goal.comments,
              goal_type: form.goalType ?? goal.goal_type,
              goal_subtype: form.goalSubtype ?? goal.goal_subtype,
            };
            this.goals = [
              ...this.goals.filter((goal) => goal.goal_id !== form.goal_id),
              updatedGoal,
            ];
            this.$patch({
              goals: this.goals,
            });
          }
        }
      }
    },
  },
});
