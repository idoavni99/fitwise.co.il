export enum WorkoutCategories {
  FullBody = 1,
  Chest = 2,
  ButtAndFeet = 3,
}

export const CategoryToName = {
  [WorkoutCategories.FullBody]: "גוף מלא",
  [WorkoutCategories.Chest]: "חזה",
  [WorkoutCategories.ButtAndFeet]: "ישבן-רגליים",
};
