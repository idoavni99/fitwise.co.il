import { EntityTypes } from "./EntityTypes";

export type User = {
  id: number;
  first_name: string;
  last_name: string;
  email: string;
  start_date?: string;
  end_date?: string;
  address?: string;
  gender?: "M" | "F" | "U";
  birthday?: string;
  entity_type: EntityTypes;
};

export enum GenderToText {
  M = "זכר",
  F = "נקבה",
  U = "אחר",
}
