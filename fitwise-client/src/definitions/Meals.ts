export enum MealTypes {
  Breakfast,
  Lunch,
  Dinner,
  Middle,
}

export type Meal = {
  meal_type: MealTypes;
  food_type: string;
  sum_calories: number;
  sum_protein: number;
  sum_carbohydrates: number;
  sum_fats: number;
  gram: number;
  date: string;
};

export type Menu = Omit<Meal, "date"> & { menu_id: number };

export type MealStats = {
  protein: number;
  carbs: number;
  calories: number;
};

export const mealTypesToText = {
  [MealTypes.Breakfast]: "בוקר",
  [MealTypes.Lunch]: "צהריים",
  [MealTypes.Dinner]: "ערב",
  [MealTypes.Middle]: "ביניים",
};

export const mealRecommendations = {
  [MealTypes.Breakfast]: 248,
  [MealTypes.Lunch]: 497,
  [MealTypes.Dinner]: 373,
  [MealTypes.Middle]: 124,
};
