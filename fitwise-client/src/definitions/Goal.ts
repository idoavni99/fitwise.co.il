export enum GoalTypes {
  NUTRITIONAL = 0,
  PHYSICAL = 1,
}

export type Goal = {
  goal_id: number;
  goal_type: number;
  goal_subtype: string;
  start_date: string;
  end_date: string;
  comments: string;
  completed: boolean;
};
