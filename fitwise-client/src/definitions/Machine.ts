import { WorkoutCategories } from "./WorkoutCategories";

export type MachineAvailabilityTime = {
  start_time: string;
  finish_time: string;
  selected_day: string;
};

export type Machine = {
  machine_id: number;
  category_id: WorkoutCategories;
  machine_name: string;
  available_time_date: string;
  available_start_hour: string;
  open_ind: boolean;
  if_class: boolean;
};
