export type TrainingProgram = {
  program_id: number;
  machine_id: number;
  machine_name: string;
  category_id: number;
  set_num: number;
  repetition: number;
  weight?: number;
  rest_mins?: number;
  comments?: number;
  description: string;
};

export type Workout = {
  machine_id: number;
  done: boolean;
  machine_name: string;
  selected_day: string;
  start_time: string;
  finish_time: string;
};
