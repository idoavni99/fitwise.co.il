import { User } from "./User";

export type Customer = User & {
  age: number;
  fullName: string;
  has_doc: boolean;
  fat_precentage?: number;
  height?: number;
  weight?: number;
  comments?: string;
  medical_notes?: string;
  target?: string;
  calendar_per: boolean;
};

export type CustomerEntry = {
  id: number;
  fullName: string;
  age: number;
};
