export enum EntityTypes {
  Customer = "customer",
  Trainer = "trainer",
  Nutritionist = "nutritionist",
}

export const EntityTypeToTitle = {
  customer: "מתאמנ/ת",
  trainer: "מאמנ/ת",
  nutritionist: "תזונאי/ת",
};
