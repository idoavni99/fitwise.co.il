// A function that wraps fetch, takes an API Route, A REST Method (POST,GET...) And a request body if needed, and will return the response from the server or an error
export const sendRequest = async <T>(
  path: string,
  method: string,
  body?: object
) => {
  const response = await fetch(
    `${process.env.VUE_APP_API_URL ?? location.origin + "/api"}/${path}`,
    {
      method,
      body: JSON.stringify(body),
      headers: {
        "Content-Type": "application/json",
      },
      credentials: "include",
    }
  );
  if (response.status !== 500) {
    return response.json() as Promise<Response<T>>;
  } else {
    throw new Error("A network error occurred");
  }
};

export type Response<T> = T & {
  status: number;
};
