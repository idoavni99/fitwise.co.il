import Vue, { markRaw } from "vue";
import App from "./App.vue";
import router from "./router";

import VueSweetalert2 from "vue-sweetalert2";
import { createPinia, PiniaVuePlugin } from "pinia";
import { BootstrapVue, IconsPlugin } from "bootstrap-vue";
import { useUserStore } from "./stores/userStore";
import "@/assets/theme.scss";
import piniaPluginPersistedstate from "pinia-plugin-persistedstate";

// Configuring the pinia store
Vue.use(PiniaVuePlugin);
const pinia = createPinia();
pinia.use(({ store }) => {
  store.router = markRaw(router);
});
pinia.use(piniaPluginPersistedstate);

// Configuring Bootstrap and Vue SweetAlert (Success Dialogs)
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueSweetalert2);

Vue.config.productionTip = false;

// Checks rotue permissions before letting user enter a screen (Employee permissions or authentication)
router.beforeEach((to, _from, next) => {
  const store = useUserStore(pinia);
  if (!to.meta?.public && !store.isAuthenticated) next("/login");
  else if (to.meta?.employeeOnly && store.isCustomer) next("/");
  else next();
});

new Vue({
  router,
  render: (h) => h(App),
  pinia,
}).$mount("#app");
