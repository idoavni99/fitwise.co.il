// Request the token's info from google and checks if it has expired.
export default async (token) => {
  const { expires_in } = await (
    await fetch(
      `https://www.googleapis.com/oauth2/v1/tokeninfo?access_token=${token}`,
      { method: "GET" }
    )
  ).json();

  return expires_in <= 0;
};
