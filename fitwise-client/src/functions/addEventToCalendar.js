// Adding an event to google calendar.
// eventPayload: includes a title of the event, it's description, start and end time.
// From which a Google Calendar event is constructed
export const addEventToCalendar = async (eventPayload) =>
  await window.gapi.client.calendar.events.insert({
    calendarId: "primary",
    resource: {
      summary: eventPayload.summary,
      description: eventPayload.description,
      start: {
        dateTime: `${eventPayload.selected_day.split("T")[0]}T${
          eventPayload.start_time
        }:00`,
        timeZone: "Asia/Jerusalem",
      },
      end: {
        dateTime: `${eventPayload.selected_day.split("T")[0]}T${
          eventPayload.finish_time
        }:00`,
        timeZone: "Asia/Jerusalem",
      },
      reminders: {
        useDefault: true,
      },
    },
  });
