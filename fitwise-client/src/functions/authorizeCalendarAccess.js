import isTokenExpired from "./isTokenExpired";
// Handles authorization to google in order to sync calendar.
// Checks for an existing authorization token and if it has not yet expired.
// if no token exists or it has expired, authenticates to google
export const authorizeCalendarAccess = async () => {
  const { access_token } = window.gapi.auth.getToken() ?? {};
  if (!access_token || (await isTokenExpired(access_token))) {
    return new Promise((resolve) => {
      const client = window.google.accounts.oauth2.initTokenClient({
        client_id:
          "472477653298-ea4vgib2u53d04e8qvg7m3njb08gjb65.apps.googleusercontent.com",
        scope:
          "email profile https://www.googleapis.com/auth/calendar.readonly https://www.googleapis.com/auth/calendar.events",
        ux_mode: "popup",
        callback: ({ access_token, error }) => {
          if (error) {
            resolve(undefined);
          }
          //Sync to calendar using API
          window.gapi.auth.setToken(access_token);
          resolve(access_token);
        },
      });
      client.requestAccessToken();
    });
  }

  return access_token;
};
