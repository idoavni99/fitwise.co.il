import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import HomeView from "@/views/Home/HomeView.vue";
Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/home",
    name: "home",
    component: HomeView,
  },
  {
    path: "/physical-data",
    name: "Physilogical-Details",
    component: () => import("@/views/PersonalInfo/PhysiologicalDataView.vue"),
  },
  {
    path: "/goals",
    name: "goals",
    component: () => import("@/views/PersonalInfo/GoalsView.vue"),
  },
  {
    path: "/login",
    name: "login",
    meta: { public: true },
    component: () => import("../views/LoginView.vue"),
  },
  {
    path: "/details",
    name: "details",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import("../views/PersonalInfo/UserDetailsView.vue"),
  },
  {
    path: "/subscribe",
    name: "purchase-subscription",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import("../views/PersonalInfo/PurchaseSubscriptionView.vue"),
  },
  {
    path: "/nutrition/preferences",
    name: "nutrition-preferences",
    component: () => import("../views/Nutrition/NutritionPreferencesView.vue"),
  },
  {
    path: "/nutrition/dailyMenu",
    name: "daily-menu",
    component: () => import("../views/Nutrition/DailyMenuView.vue"),
  },
  {
    path: "/nutrition/meals/:mealType",
    name: "meals",
    component: () => import("../views/Nutrition/MealView.vue"),
  },
  {
    path: "/nutrition/meals/:mealType/save",
    name: "save-meal",
    component: () => import("../views/Nutrition/SaveMealView.vue"),
  },
  {
    path: "/trainingPlans/view",
    name: "view-training-plans",
    component: () => import("../views/TrainingPlans/TrainingPlansView.vue"),
  },
  {
    path: "/trainingPlans/view/:categoryId",
    name: "view-workout",
    component: () => import("../views/TrainingPlans/WorkoutView.vue"),
  },
  {
    path: "/trainingPlans/register/:machineId?",
    props: true,
    name: "machine-registration",
    component: () =>
      import("../views/TrainingPlans/MachineRegistrationView.vue"),
  },
  {
    path: "/workouts",
    name: "my-workouts",
    component: () => import("../views/TrainingPlans/MyWorkoutsView.vue"),
  },
  {
    path: "/customers",
    name: "customers-view",
    component: () => import("../views/Employee/Customers/CustomerView.vue"),
    children: [
      {
        path: "details",
        name: "customers-details",
        component: () =>
          import("@/views/Employee/Customers/CustomerDetails.vue"),
        meta: { employeeOnly: true, hideFooter: true, hideHeader: true },
      },
      {
        path: "goals",
        name: "customers-goals",
        component: () => import("@/views/Employee/Trainer/CustomerGoals.vue"),
        meta: { employeeOnly: true, titleKey: "goals" },
      },
      {
        path: "trainingPlans",
        name: "trainingPlans",
        component: () =>
          import(
            "@/views/Employee/Trainer/TrainingPlans/ViewCustomerPlans.vue"
          ),
        meta: { employeeOnly: true, titleKey: "trainingPlans" },
      },
      {
        path: "trainingPlans/:categoryId",
        name: "trainingPlans-workout",
        props: true,
        component: () =>
          import("@/views/Employee/Trainer/TrainingPlans/ViewCustomerPlan.vue"),
        meta: {
          employeeOnly: true,
          hideFooter: true,
          titleKey: "trainingPlans",
        },
      },
      {
        path: "menus",
        name: "menus",
        component: () =>
          import("@/views/Employee/Nutritionist/BuildMenuView.vue"),
        meta: { employeeOnly: true, titleKey: "menus" },
      },
      {
        path: "menus/:mealId",
        name: "menus-meal",
        props: true,
        component: () =>
          import("@/views/Employee/Nutritionist/MealMenuView.vue"),
        meta: { employeeOnly: true, hideFooter: true, titleKey: "menus" },
      },
      {
        path: "menus/:mealId/add",
        name: "menus-addMeal",
        props: true,
        component: () =>
          import("@/views/Employee/Nutritionist/AddMealView.vue"),
        meta: { employeeOnly: true, hideFooter: true, titleKey: "menus" },
      },
    ],
  },
  {
    path: "*",
    redirect: "/home",
  },
];

const router = new VueRouter({
  mode: process.env.NODE_ENV === "development" ? "history" : "hash",
  base: process.env.BASE_URL,
  routes,
});

export default router;
