## Pre-Requisits

Have Node.js installed.
Have MYSQL Workbench 8.0 Or any other MYSQL GUI installed
Import the dump from fitwise_db in MYSQL.
Add a user with the name backend and a password of your choice to the DB.

## How to start up the server

1. run npm install

2. create a new .env file with this data:

PORT=3306
SERVER_PORT=3003
DATABASE=fitwise_db
USER=YOUR_USERNAME
HOST=localhost
PASSWORD=YOUR_PASSWORD

3. run npm run dev

4. Viola. server is now running
