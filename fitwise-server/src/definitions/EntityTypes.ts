export enum EntityTypes {
  Customer = "customer",
  Nutritionist = "nutritionist",
  Trainer = "trainer",
}
