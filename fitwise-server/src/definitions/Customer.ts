import { User } from "./User";

export type Customer = User & {
  has_doc: boolean;
  fat_precentage?: number;
  height?: number;
  weight?: number;
  comments?: string;
  calendar_per: boolean;
  medical_notes: string;
  target: string;
};
