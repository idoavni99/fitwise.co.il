export enum MealTypes {
  Breakfast,
  Lunch,
  Dinner,
  Middle,
}

export type Meal = {
  meal_type: MealTypes;
  food_type: string;
  sum_calories: number;
  sum_protein: number;
  sum_carbohydrates: number;
  sum_fats: number;
  gram: number;
};
