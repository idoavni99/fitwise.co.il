export type TrainingProgram = {
  program_id: number;
  machine_name: number;
  category_id: number;
  set_num: number;
  repetition: number;
  weight?: number;
  rest_mins?: number;
  comments?: number;
  description: string;
};
