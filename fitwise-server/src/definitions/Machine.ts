export type Machine = {
  machine_id: number;
  category_id: number;
  machine_name: string;
  available_time_date: string;
  available_start_hour: string;
  open_ind: boolean;
  if_class: boolean;
};
