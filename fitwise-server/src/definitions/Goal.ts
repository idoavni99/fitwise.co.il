export enum GoalTypes {
  NUTRITIONAL = 0,
  PHYSICAL = 1,
}

export type Goal = {
  goal_type: number;
  goal_subtype: number;
  start_date: Date;
  end_date: Date;
  comments: string;
};
