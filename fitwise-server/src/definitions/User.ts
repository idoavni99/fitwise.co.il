import { EntityTypes } from "./EntityTypes";

export type User = {
  id: number;
  password: string;
  first_name?: string;
  last_name?: string;
  email: string;
  start_date: Date;
  end_date?: Date;
  address?: string;
  gender?: "M" | "F" | "U";
  birthday?: Date;
  entity_type: EntityTypes;
};
