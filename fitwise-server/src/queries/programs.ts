const programsQueries = {
  findByCategoryId: `SELECT generic_training_programs.*, machines.machine_name FROM generic_training_programs LEFT JOIN machines ON generic_training_programs.machine_id = machines.machine_id WHERE generic_training_programs.category_id = :id`,
  findByCategoryAndCustomerId: `SELECT customers_training_plans.*, machines.machine_name FROM customers_training_plans LEFT JOIN machines ON customers_training_plans.machine_id = machines.machine_id WHERE customers_training_plans.category_id = :id AND customers_training_plans.customer_id = :customerId`,
  findOneByPlanId: `SELECT * from customers_training_plans WHERE program_id = :program_id`,
  findMachines: `SELECT * from machines`,
  findMachineTimesByDate: `SELECT start_time, finish_time FROM machine_save_times WHERE machine_id = :id AND selected_day = :date`,
  findWorkoutsByCustomerId: `SELECT machine_save_times.start_time, machine_save_times.finish_time, machine_save_times.machine_id, machine_save_times.done, machines.machine_name FROM machine_save_times LEFT JOIN machines ON machine_save_times.machine_id = machines.machine_id WHERE machine_save_times.selected_day = :date AND machine_save_times.customer_id = :id`,
  setWorkoutDone: `UPDATE machine_save_times SET done = :done WHERE selected_day = :selected_day AND start_time = :start_time AND finish_time = :finish_time AND machine_id = :machine_id`,
  findMachineTimesThisWeekByCustomerId: `SELECT * FROM machine_save_times WHERE customer_id = :id AND YEARWEEK(selected_day, '1') = YEARWEEK(NOW(), '1')`,
  reserveMachine: `INSERT INTO machine_save_times (customer_id, machine_id, selected_day, start_time, finish_time) VALUES (:id, :machine_id, :selected_day, :start_time, :finish_time)`,
  addTrainingPlan: `INSERT INTO customers_training_plans (customer_id, category_id, machine_id, comments, description, repetition, set_num, rest_mins) VALUES (:customer_id, :category_id, :machine_id, :comments, :description, :repetition, :set_num, :rest_mins)`,
};

export default programsQueries;
