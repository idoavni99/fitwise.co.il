const employeeQueries = {
  findCustomerEntries: `SELECT id, TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age, CONCAT_WS(" ", first_name, last_name) AS 'fullName' FROM users WHERE entity_type = 'customer'`,
};
export default employeeQueries;
