const customerQueries = {
  updateCustomer: `UPDATE customers SET fat_precentage = :fatPrecentage, height = :height, weight = :weight, comments = :comments, calendar_per = :calendarPer, medical_notes = :medical_notes, target = :target WHERE customer_id = :id`,
  createCustomer: `INSERT INTO customers (customer_id, has_doc) values (?, ?)`,
  findGoalsById: `SELECT goal_id, goal_type, goal_subtype, start_date, end_date, comments, completed FROM customers_goals WHERE customer_id = :id`,
  createGoal: `INSERT INTO customers_goals (customer_id, goal_type, goal_subtype, start_date, end_date, comments) VALUES (:id, :goalType, :goalSubtype, :startDate, :endDate, :comments)`,
  removeAllPreferences: `DELETE FROM customers_preferences WHERE customer_id = :id`,
  addPreferences: `INSERT INTO customers_preferences (customer_id, preference_type, preference_subtype) VALUES ?`,
  getPreferences: `SELECT preference_type, preference_subtype FROM customers_preferences WHERE customer_id = :id`,
  getMealsByDate: `SELECT meal_type, sum_calories, food_type ,sum_protein, sum_carbohydrates, sum_fats, gram, date FROM customers_menus WHERE customer_id = :id AND date = :date`,
  getMealsWeeklyStats: `SELECT SUM(sum_protein) as protein, SUM(sum_carbohydrates) as carbs, SUM(sum_calories) as calories FROM customers_menus WHERE customer_id = :id AND YEARWEEK(date, '1') = YEARWEEK(NOW(), '1')`,
  findMeals: `SELECT * FROM generic_menus WHERE customer_id IS NULL OR customer_id = :id`,
  addMeal: `INSERT INTO customers_menus (customer_id, date, meal_type, sum_calories, food_type, sum_protein, sum_carbohydrates, sum_fats, gram) VALUES (:id, :date, :meal_type, :sum_calories, :food_type, :sum_protein, :sum_carbohydrates, :sum_fats, :gram)`,
  addCustomerMeal: `INSERT INTO generic_menus (customer_id, meal_type, sum_calories, food_type, sum_protein, sum_carbohydrates, sum_fats, gram) VALUES (:customer_id, :meal_type, :sum_calories, :food_type, :sum_protein, :sum_carbohydrates, :sum_fats, :gram)`,
  updateMeal: `UPDATE customers_menus SET sum_calories = :sum_calories, food_type= :food_type, sum_protein = :sum_protein, sum_carbohydrates = :sum_carbohydrates, sum_fats = :sum_fats, gram = :gram WHERE customer_id = :id AND date = :date AND meal_type = :meal_type`,
};

export default customerQueries;
