const userQueries = {
  findOneByEmail: `SELECT * FROM users WHERE email = ?`,
  findCustomerById: `SELECT *, TIMESTAMPDIFF(YEAR, birthday, CURDATE()) AS age, CONCAT_WS(" ", first_name, last_name) AS 'fullName' FROM users LEFT JOIN customers ON id = customers.customer_id WHERE id = :id`,
  createNewUser: `INSERT INTO users (id, email, password, first_name, last_name, birthday, gender, address) values (?, ?, ?, ?, ?, ?, ?, ?)`,
  updateUser: `UPDATE users SET first_name = :firstName, last_name = :lastName, birthday = :birthday, gender = :gender, address = :address, end_date = :endDate WHERE id = :id`,
  deleteUser: `DELETE FROM users where id = ?`,
};

export default userQueries;
