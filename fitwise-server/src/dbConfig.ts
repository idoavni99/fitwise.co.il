// Connection data from enviroment variables with default fallbacks
export const DATA_SOURCES = {
  mySqlDataSource: {
    host: process.env.HOST ?? "localhost",
    user: process.env.DB_USER ?? "backend",
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.PORT ?? "3306"),
    database: process.env.DATABASE ?? "fitwise",
    connectionLimit: process.env.CONNECTION_LIMIT
      ? parseInt(process.env.MY_SQL_DB_CONNECTION_LIMIT)
      : 4,
  },
};
