import { find, insert } from "mysql.connector";
import customerQueries from "queries/customers";
import employeeQueries from "queries/employees";
import employeesRouter from ".";

type CustomerEntry = {
  id: number;
  age: number;
  fullName: string;
};

// Gets customers for employees
employeesRouter.route("/customers").get(async (req, res) => {
  if (!req.session.employeeKey) {
    return res.status(401).json({ error: "Not Authorized" });
  }

  const customers = await find<CustomerEntry>(
    employeeQueries.findCustomerEntries
  );

  return res.status(200).json({ customers });
});

// Add menu for customer
employeesRouter.route("/customers/meals").put(async (req, res) => {
  if (!req.session.employeeKey) {
    return res.status(401).json({ error: "Not Authorized" });
  }

  const {
    customer_id = null,
    meal_type,
    food_type,
    sum_calories = 0,
    sum_protein = 0,
    sum_carbohydrates = 0,
    sum_fats = 0,
    gram = 0,
  } = req.body;

  if (customer_id && meal_type !== undefined && food_type) {
    const menu_id = await insert(customerQueries.addCustomerMeal, {
      customer_id,
      meal_type,
      food_type,
      sum_calories,
      sum_protein,
      sum_carbohydrates,
      sum_fats,
      gram,
    });

    if (typeof menu_id === "number") {
      return res.status(200).json({ menu: { ...req.body, menu_id } });
    }
  }

  return res.sendStatus(500);
});
