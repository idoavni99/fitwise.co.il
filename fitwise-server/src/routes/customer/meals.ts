import { EntityTypes } from "definitions/EntityTypes";
import { Meal, MealTypes } from "definitions/Meals";
import { find, findOne, insert, update } from "mysql.connector";
import customerQueries from "queries/customers";
import customerRouter from ".";

// Get generic menus and customer-specific menus.
customerRouter.route("/meals/menus").get(async (req, res) => {
  const { customerId } = req.query;
  console.log(customerId, req.session.userId, req.session.entityType);
  const result = await find<Meal>(customerQueries.findMeals, {
    id:
      req.session.entityType === EntityTypes.Nutritionist
        ? customerId
        : req.session.userId,
  });
  const meals = result.reduce(
    (meals, meal) => {
      return {
        ...meals,
        [meal.meal_type]: meals[meal.meal_type].concat(meal),
      };
    },
    {
      [MealTypes.Breakfast]: [],
      [MealTypes.Lunch]: [],
      [MealTypes.Dinner]: [],
      [MealTypes.Middle]: [],
    }
  );

  return res.status(200).json({ meals });
});

// meal actions for specific day (get, add, update)
customerRouter
  .route("/:id/meals/:date")
  .get(async (req, res) => {
    const { id, date } = req.params;
    console.info("get meals request Params", id, date);

    const meals = await find<Meal>(customerQueries.getMealsByDate, {
      id,
      date,
    });
    if (meals) {
      return res.status(200).json({ meals });
    }

    return res.status(500).json({ message: "An Error occurred" });
  })
  .put(async (req, res) => {
    const { id, date } = req.params;
    const {
      meal_type,
      sum_calories,
      food_type,
      sum_protein,
      sum_carbohydrates,
      sum_fats,
      gram,
    } = req.body;
    console.info("insert meal request Params", id, date);
    const result = await insert(customerQueries.addMeal, {
      id,
      date,
      meal_type,
      sum_calories,
      food_type,
      sum_protein,
      sum_carbohydrates,
      sum_fats,
      gram,
    });

    if (result !== false) {
      return res.status(200).json({
        meal: {
          meal_type,
          sum_calories,
          food_type,
          sum_protein,
          sum_carbohydrates,
          sum_fats,
          gram,
          date,
          id: result,
          customer_id: id,
        },
      });
    }

    return res.status(500).json({ message: "Failed to add meals" });
  })
  .patch(async (req, res) => {
    const { id, date } = req.params;
    const {
      meal_type,
      sum_calories,
      food_type,
      sum_protein,
      sum_carbohydrates,
      sum_fats,
      gram,
    } = req.body;
    console.info("replace meal request Params", id, date);

    const result = await update(customerQueries.updateMeal, {
      id,
      date,
      meal_type,
      sum_calories,
      food_type,
      sum_protein,
      sum_carbohydrates,
      sum_fats,
      gram,
    });

    if (result !== false) {
      return res.status(200).json({
        meal: {
          meal_type,
          sum_calories,
          food_type,
          sum_protein,
          sum_carbohydrates,
          sum_fats,
          gram,
          date,
          id: result,
          customer_id: id,
        },
      });
    }

    return res.status(500).json({ err: "an error occurred" });
  });

// Get weekly food statistics for customer
customerRouter.route("/:id/weeklyStats").get(async (req, res) => {
  const { id } = req.params;
  const result = await findOne(customerQueries.getMealsWeeklyStats, { id });

  if (result) {
    res.status(200).json({ stats: result });
  }
});
