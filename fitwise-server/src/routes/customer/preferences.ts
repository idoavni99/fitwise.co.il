import { Request } from "express";
import { remove, insert, find } from "mysql.connector";
import customerQueries from "queries/customers";
import customerRouter from ".";

type Body = {
  favouriteFoods: string[];
  noGiveUpFoods: string[];
};

enum PreferenceTypes {
  noGiveUp,
  favourtie,
}

// Add/Update preferences, get perferences
customerRouter
  .route("/:id/preferences")
  .post(async (req: Request<{ id: string }, {}, Body>, res) => {
    const { id } = req.params;
    const { favouriteFoods = [], noGiveUpFoods = [] } = req.body;

    await remove(customerQueries.removeAllPreferences, {
      id,
    });

    const result = await insert(customerQueries.addPreferences, [
      favouriteFoods
        .map((item) => [id, PreferenceTypes.favourtie, item])
        .concat(
          noGiveUpFoods.map((item) => [id, PreferenceTypes.noGiveUp, item])
        ),
    ]);

    if (result !== false) {
      return res.status(200).json({ favouriteFoods, noGiveUpFoods });
    }

    return res.status(500).json({ message: "Unexpected Error" });
  })
  .get(async (req, res) => {
    const { id } = req.params;
    const items = await find<{
      preference_type: PreferenceTypes;
      preference_subtype: string;
    }>(customerQueries.getPreferences, { id });
    if (items) {
      const favouriteFoods = items
        .filter((item) => item.preference_type === PreferenceTypes.favourtie)
        .map((item) => item.preference_subtype);
      const noGiveUpFoods = items
        .filter((item) => item.preference_type === PreferenceTypes.noGiveUp)
        .map((item) => item.preference_subtype);

      return res.status(200).json({ favouriteFoods, noGiveUpFoods });
    }

    return res.status(500).json({ message: "Unexpected Error" });
  });
