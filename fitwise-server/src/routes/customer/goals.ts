import { Goal } from "definitions/Goal";
import { find, insert, update } from "mysql.connector";
import { parseTimestamp } from "operations/parseTimestamp";
import customerQueries from "queries/customers";
import customerRouter from ".";

// Goal actions for customer by id (get goals, add goal, update goal)
customerRouter
  .route("/:id/goals")
  .get(async (req, res) => {
    const { id } = req.params;

    const goals = await find<Goal>(customerQueries.findGoalsById, { id });

    if (goals) {
      return res.status(200).json({ status: 200, goals });
    }

    return res.status(404).json({ status: 404, error: "Goals not found" });
  })
  .put(async (req, res) => {
    const { id } = req.params;
    const {
      goalType = null,
      goalSubtype = null,
      startDate = new Date(),
      endDate = null,
      comments = null,
    } = req.body;

    console.log("Incoming add goal body", req.body);

    const result = await insert(customerQueries.createGoal, {
      goalType,
      goalSubtype,
      startDate,
      endDate: parseTimestamp(endDate),
      comments,
      id,
    });

    if (result !== false) {
      return res.status(200).send({
        message: "Goal inserted",
        goal: {
          goal_type: goalType,
          goal_subtype: goalSubtype,
          start_date: startDate,
          end_date: endDate,
          comments,
          goal_id: result,
          customer_id: id,
        },
      });
    }

    return res.status(500).json({ message: "Goal not inserted properly" });
  })
  .patch(async (req, res) => {
    const { id } = req.params;
    const { endDate, comments, completed, goal_id } = req.body;

    if (id) {
      const updateQuery = `UPDATE customers_goals SET${
        endDate ? " end_date = :endDate" : ""
      }${comments ? ",comments = :comments" : ""}${
        completed !== undefined ? ",completed = :completed" : ""
      } WHERE customer_id = :id AND goal_id = :goal_id`;

      console.log("AWAITING UPDATE", updateQuery);

      const result = await update(updateQuery, {
        endDate: parseTimestamp(endDate),
        comments,
        id,
        goal_id,
        completed,
      });

      if (result) {
        return res.status(200).send({ message: "Goal updated" });
      }
    }

    return res.status(500).json({ message: "Goal not updated properly" });
  });
