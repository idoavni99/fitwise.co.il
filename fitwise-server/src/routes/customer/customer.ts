import { Customer } from "definitions/Customer";
import { findOne, remove, update } from "mysql.connector";
import customerQueries from "queries/customers";
import userQueries from "queries/users";
import customerRouter from "./index";

// Customer actions (get details, update physiological data)
customerRouter
  .route("/:id")
  .get(async (req, res) => {
    const user = await findOne<Customer>(userQueries.findCustomerById, {
      id: req.params.id,
    });

    if (user) {
      delete user.password;
      res.status(200).json({ status: 200, ...user });
      return;
    }

    res.status(404).json({ message: "User not found" });
  })
  .patch(async (req, res) => {
    const { id } = req.params;
    try {
      const user = await findOne<Customer>(userQueries.findCustomerById, {
        id,
      });
      const {
        fatPrecentage = user.fat_precentage ?? null,
        height = user.height ?? null,
        weight = user.weight ?? null,
        comments = user.comments ?? null,
        medical_notes = user.medical_notes ?? null,
        target = user.target ?? null,
        calendarPer = user.calendar_per ?? false,
        ...userData
      } = req.body;

      if (userData.firstName) {
        await update(userQueries.updateUser, {
          firstName: userData.firstName ?? null,
          lastName: userData.lastName ?? null,
          birthday: userData.birthday,
          address: userData.address ?? null,
          gender: userData.gender ?? "U",
          endDate: userData.endDate ?? null,
          id,
        });
      } else {
        await update(customerQueries.updateCustomer, {
          id,
          fatPrecentage,
          height,
          weight,
          comments,
          medical_notes,
          target,
          calendarPer,
        });
      }
      console.log("FINISH UPDATE PHASE");
      const updatedUser = await findOne<Customer>(
        userQueries.findCustomerById,
        {
          id,
        }
      );
      res.status(200).json({ status: 200, ...updatedUser });
    } catch (error) {
      res.status(500).json({ error });
    }
  });
