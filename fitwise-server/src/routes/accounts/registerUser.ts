import { User } from "definitions/User";
import { findOne, insert } from "mysql.connector";
import userQueries from "queries/users";
import customerQueries from "queries/customers";
import accountsRouter from ".";
import { parseTimestamp } from "operations/parseTimestamp";

// Create new user (currently not in use)
accountsRouter.route("/register").post(async (req, res) => {
  const {
    email,
    password,
    id,
    firstName,
    lastName,
    gender,
    documents,
    address,
    birthday,
  } = req.body;
  console.log("Registering User", req.body);
  const existingUser = await findOne<User>(userQueries.findOneByEmail, [email]);
  if (!existingUser) {
    await insert(userQueries.createNewUser, [
      id,
      email,
      password,
      firstName,
      lastName,
      parseTimestamp(birthday as string),
      gender,
      address,
    ]);
    await insert(customerQueries.createCustomer, [id, documents.length]);
    const user = await findOne<User>(userQueries.findOneByEmail, [email]);
    if (user) {
      delete user.password;
      return res.status(200).json({ status: 200, ...user });
    }
  } else {
    delete existingUser.password;
    return res.status(200).json({ status: 200, ...existingUser });
  }

  return res.status(500).json({ message: "Internal Server Error" });
});
