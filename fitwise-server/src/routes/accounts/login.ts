import { LoginMessages, validateUserLogin } from "operations/validateUserLogin";
import router from ".";

// Login user with email and password (set userId and entityType in session for later use)
router.route("/login").post(async (req, res) => {
  const { email, password } = req.body;
  const result = await validateUserLogin(email, password);
  if (result.status === LoginMessages.SUCCESS) {
    req.session.userId = result.id;
    req.session.entityType = result.entity_type;
    if (result.isEmployee) {
      req.session.employeeKey = result.id;
    }
  }
  res.status(result.status).json(result);
});
