import { EntityTypes } from "definitions/EntityTypes";
import { TrainingProgram } from "definitions/TrainingProgram";
import { find, findOne, insert, update } from "mysql.connector";
import programsQueries from "queries/programs";
import programsRouter from ".";

// Get training programs by category (generic & customer-specific)
programsRouter.route("/category/:id").get(async (req, res) => {
  const { id } = req.params;
  const { customerId } = req.query;

  if (customerId) {
    if (
      !req.session?.employeeKey &&
      req.session.entityType !== EntityTypes.Trainer
    ) {
      return res.status(403).json({ status: 403, error: "not a trainer" });
    }
  }

  console.log("programs by category - ", id);
  const programs = !customerId
    ? await find<TrainingProgram>(programsQueries.findByCategoryId, { id })
    : [];

  const customerPrograms = await find<TrainingProgram>(
    programsQueries.findByCategoryAndCustomerId,
    { id, customerId: customerId ?? req.session?.userId }
  );

  if (programs) {
    return res
      .status(200)
      .json({ status: 200, programs: [...programs, ...customerPrograms] });
  }

  return res.status(500).json({ status: 500, error: "An Error occurred" });
});
// Save training program (add&update)
programsRouter.route("/save").post(async (req, res) => {
  const {
    comments,
    description,
    repetition,
    machine_id,
    program_id,
    set_num,
    rest_mins,
  } = req.body;

  if (
    !req.session?.employeeKey &&
    req.session.entityType !== EntityTypes.Trainer
  ) {
    return res.status(403).json({ status: 403, error: "not a trainer" });
  }
  let program;

  if (program_id) {
    const updateQuery = `UPDATE customers_training_plans SET${
      description ? " description = :description" : ""
    }
    ${comments ? ",comments = :comments" : ""}
    ${set_num ? ",set_num = :set_num" : ""}
    ${rest_mins ? ",rest_mins = :rest_mins" : ""}
    ${machine_id ? ",machine_id = :machine_id" : ""}
    ${
      repetition ? ",repetition = :repetition" : ""
    } WHERE program_id = :program_id`;

    console.log("AWAITING UPDATE", updateQuery);

    const success = await update(updateQuery, {
      description,
      comments,
      set_num,
      rest_mins,
      machine_id,
      repetition,
      program_id,
    });
    if (success) {
      program = await findOne(programsQueries.findOneByPlanId, { program_id });
    }
  } else {
    const planId = await insert(programsQueries.addTrainingPlan, req.body);
    if (typeof planId === "number") {
      program = await findOne(programsQueries.findOneByPlanId, {
        program_id: planId,
      });
    }
  }
  if (program) {
    return res.status(200).json({ program });
  }
  return res.sendStatus(500);
});
