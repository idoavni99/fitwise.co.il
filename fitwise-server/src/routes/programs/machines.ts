import { find, insert, update } from "mysql.connector";
import programsQueries from "queries/programs";
import programsRouter from ".";

// Find available machines
programsRouter.route("/machines").get(async (_req, res) => {
  const machines = await find(programsQueries.findMachines, {});

  return res.status(200).json({ machines });
});

// Get Machine reservation by date
programsRouter.route("/machines/times/:id/:date").get(async (req, res) => {
  const { date, id } = req.params;

  console.log("GET MACHINE TIMES", { id, date });

  const times = await find(programsQueries.findMachineTimesByDate, {
    id,
    date,
  });

  return res.status(200).json({ times });
});

// Get machine reservations by customer
programsRouter
  .route("/customer/machines/times/:id")
  .get(async (req, res) => {
    const { id } = req.params;

    const times = await find(
      programsQueries.findMachineTimesThisWeekByCustomerId,
      {
        id,
      }
    );

    return res.status(200).json({ times });
  })
  .put(async (req, res) => {
    const { id = null } = req.params;
    const {
      machine_id = null,
      selected_day = null,
      start_time = null,
      finish_time = null,
    } = req.body;

    console.log("Inserting save time", { id, ...req.body });

    const result = await insert(programsQueries.reserveMachine, {
      id,
      machine_id,
      selected_day: selected_day.split("T")[0],
      start_time,
      finish_time,
    });

    if (result !== false) {
      return res.status(200).json({ message: "success" });
    }

    return res.status(500).json({ message: "error" });
  });

// Get workout plans by customer id for specific day
programsRouter.route("/workouts/:id/:date").get(async (req, res) => {
  const { id, date } = req.params;

  const workouts = await find(programsQueries.findWorkoutsByCustomerId, {
    id,
    date,
  });

  if (workouts !== undefined) {
    return res.status(200).json({ message: "success", workouts });
  }

  return res.status(500).json({ message: "error" });
});

// Set workout as done
programsRouter.route("/workouts/setDone").patch(async (req, res) => {
  if (req.body.workout) {
    const result = await update(programsQueries.setWorkoutDone, {
      customer_id: req.session?.userId ?? null,
      ...req.body.workout,
    });

    if (result !== undefined) {
      return res.status(200).json({ message: "success" });
    }
  }

  return res.status(500).json({ message: "error" });
});
