import "dotenv/config";
import express, { json, urlencoded } from "express";
import session from "cookie-session";
import * as MYSQLConnector from "mysql.connector";
import accountsRotuer from "routes/accounts";
import customerRouter from "routes/customer";
import "routes/accounts/routes";
import "routes/customer/routes";
import "routes/programs/routes";
import "routes/employees/routes";

import cors from "cors";
import programsRouter from "routes/programs";
import employeesRouter from "routes/employees";
const app = express();
const port = process.env.SERVER_PORT ?? 5000;

// If the project is build for prod, trust the apache server.
if (process.env.NODE_ENV === "production") app.set("trust-proxy", "loopback");
// Allow the client to save cookies and access the server
app.use(cors({ credentials: true, origin: process.env.CLIENT_URL }));
// Parse data from client as json.
app.use(json());
// Parse url encoded data
app.use(urlencoded({ extended: true }));
// Store user/employee credentials securely
app.use(
  session({
    name: "credentialStore",
    secret: "h12121",
  })
);

// Routes that accept requests, on the left is the base url, on the right, the router itself.
app.use("/accounts", accountsRotuer);
app.use("/customer", customerRouter);
app.use("/programs", programsRouter);
app.use("/employees", employeesRouter);

// Connect to MYSQL
MYSQLConnector.init();

app.listen(port, () => {
  return console.log(`Express is listening at http://localhost:${port}`);
});
