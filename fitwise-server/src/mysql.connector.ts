import { DATA_SOURCES } from "dbConfig";
import { createPool, Pool } from "mysql2/promise";
import { ResultSetHeader } from "mysql2/typings/mysql";
let pool: Pool;

const isResultSetHeader = (result: any): result is ResultSetHeader =>
  "insertId" in result;

/**
 * generates pool connection to be used throughout the app
 */
export const init = async () => {
  try {
    // Creates a set of DB connections to use
    pool = createPool({
      ...DATA_SOURCES.mySqlDataSource,
      namedPlaceholders: true,
      waitForConnections: true,
      connectionLimit: 10,
      queueLimit: 0,
    });
    // Check if connection is valid and release it afterwards
    const connection = await pool.getConnection();
    if (connection) {
      console.debug("MySql Adapter Pool generated successfully");
      connection.release();
    }
  } catch (error) {
    console.error("[mysql.connector][init][Error]: ", error);
    throw new Error("failed to initialized pool");
  }
};

/**
 * executes find one queries in MySQL db
 *
 * @param {string} query - provide a valid SQL query
 * @param {string[] | Object} params - provide the parameterized values used
 * in the query
 * @returns {any} a single result
 */
export const findOne = async <T>(
  query: string,
  params: string[] | Object
): Promise<T | undefined> => {
  try {
    const [rows] = await pool.query(query, params);
    console.log("FIND ONE RESULT", rows);
    return rows[0] as T | undefined;
  } catch (error) {
    console.error("[mysql.connector][execute][Error]: ", error);
  }
};

/**
 * executes find many queries in MySQL db
 *
 * @param {string} query - provide a valid SQL query
 * @param {string[] | Object} params - provide the parameterized values used
 * in the query
 * @returns {Array<any>} array of results
 */
export const find = async <T>(
  query: string,
  params?: string[] | Object
): Promise<T[] | undefined> => {
  try {
    const [rows] = await pool.query(query, params);
    console.log("FIND RESULT", rows);
    return rows as T[] | undefined;
  } catch (error) {
    console.error("[mysql.connector][execute][Error]: ", error);
  }
};

/**
 * executes insert queries in MySQL db
 *
 * @param {string} query - provide a valid SQL query
 * @param {string[] | Object} params - provide the parameterized values used
 * in the query
 * @returns {number | string | boolean} return an id or true if no id is present, or false if insert failed
 */
export const insert = async (
  query: string,
  params: string[] | Object
): Promise<number | string | boolean> => {
  try {
    if (!pool)
      throw new Error(
        "Pool was not created. Ensure pool is created when running the app."
      );
    const [rowData, fields] = await pool.query(query, params);
    console.log("INSERTED", rowData);
    return isResultSetHeader(rowData) ? rowData.insertId : true;
  } catch (error) {
    console.error("[mysql.connector][execute][Error]: ", error);
    return false;
  }
};

/**
 * executes update queries in MySQL db
 *
 * @param {string} query - provide a valid SQL query
 * @param {string[] | Object} params - provide the parameterized values used
 * in the query
 * @returns {boolean} true if updated, false if update failed
 */
export const update = async (
  query: string,
  params: object
): Promise<boolean> => {
  try {
    console.log("PARAMS to update", params);
    const [rows, fields] = await pool.execute(query, params);
    console.log("UPDATED", rows, fields);
    return true;
  } catch (error) {
    console.error("[mysql.connector][execute][Error]: ", error);
    return false;
  }
};
/**
 * executes delete queries in MySQL db
 *
 * @param {string} query - provide a valid SQL query
 * @param {string[] | Object} params - provide the parameterized values used
 * in the query
 */
export const remove = async (query: string, params: object): Promise<void> => {
  try {
    const [rows, fields] = await pool.execute(query, params);
    console.log("REMOVED", rows, fields);
  } catch (error) {
    console.error("[mysql.connector][execute][Error]: ", error);
  }
};
