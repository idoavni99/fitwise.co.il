// Convert a timestamp from javascript format to MYSQL format
export const parseTimestamp = (ts: string = new Date().toISOString()) =>
  ts?.replace("T", " ")?.replace("Z", "");
