import { Customer } from "definitions/Customer";
import { User } from "definitions/User";
import { findOne } from "mysql.connector";
import userQueries from "queries/users";

export enum LoginMessages {
  SUCCESS = 200,
  INVALID_LOGIN = 401,
  NOT_FOUND = 404,
}

type ValidationResult =
  | ({ status: LoginMessages.SUCCESS; isEmployee: boolean } & (User | Customer))
  | { status: LoginMessages.NOT_FOUND | LoginMessages.INVALID_LOGIN };

// Checks if user exists, if it's password matches and if it's an employee or not.
// Will return an error in case something is invalid.
export const validateUserLogin = async (
  email: string,
  password: string
): Promise<ValidationResult> => {
  try {
    const user = await findOne<User>(userQueries.findOneByEmail, [email]);
    if (!user) {
      return { status: LoginMessages.NOT_FOUND };
    }

    if (user?.password === password) {
      delete user.password;
      if (user.entity_type === "customer") {
        const customerData = await findOne<Customer>(
          userQueries.findCustomerById,
          { id: user.id }
        );
        return {
          ...customerData,
          status: LoginMessages.SUCCESS,
          isEmployee: false,
        };
      }
      return { status: LoginMessages.SUCCESS, isEmployee: true, ...user };
    }

    return { status: LoginMessages.INVALID_LOGIN };
  } catch (error) {
    console.error(error);
    return { status: 500 };
  }
};
