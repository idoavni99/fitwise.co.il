## How to setup the DB

1. Open MYSQL Workbench and ensure you have a server running.

2. click on your server (Local Instance MYSQL80)

3. Click on Server -> Data Import in the upper navbar.

4. Check Import from Self-Contained File

5. Load the Dump\*\*\*\*.sql file from this folder.

6. in the Default Target Schema field Click on New and write fitwise_db

7. Click on Start Import.

8. You have the entire DB Loaded
