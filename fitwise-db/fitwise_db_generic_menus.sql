-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: smartfit_db
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `generic_menus`
--

DROP TABLE IF EXISTS `generic_menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `generic_menus` (
  `menu_id` int NOT NULL AUTO_INCREMENT,
  `meal_type` int NOT NULL,
  `food_type` varchar(45) NOT NULL,
  `gram` int NOT NULL,
  `sum_calories` int NOT NULL,
  `sum_carbohydrates` int NOT NULL,
  `sum_protein` int NOT NULL,
  `sum_fats` int NOT NULL,
  `customer_id` int DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  UNIQUE KEY `menu_id_UNIQUE` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generic_menus`
--

LOCK TABLES `generic_menus` WRITE;
/*!40000 ALTER TABLE `generic_menus` DISABLE KEYS */;
INSERT INTO `generic_menus` VALUES (1,0,'ביצה מקושקשת, טונה, סלט ירוק, קרקר כוסמין',260,270,70,160,50,NULL),(2,1,'חזה עוף, שעועית ירוקה, סלט עגבניות',550,640,190,80,90,NULL),(3,1,'מוסר ים בתנור, אורז מלא, ירקות בתנור',700,810,310,130,95,NULL),(4,2,'ביצה שלמה מבושלת-מטוגנת,גזר,לחם קל',170,230,70,90,40,NULL),(5,2,'ביצה קשה,עגבניות שרי,לחם קל, גבינה לבנה',220,283,70,140,55,NULL),(6,3,'תפוח עץ, לחם קל, ריבת תות',150,260,100,0,90,NULL),(7,3,'פריכיות אורז, קולה זירו, חטיף חלבון',150,150,30,90,20,NULL),(8,0,'ביצה קשה,לחם קל, מלפפון, גבינה לבנה',200,210,70,120,30,NULL);
/*!40000 ALTER TABLE `generic_menus` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-11 18:33:17
