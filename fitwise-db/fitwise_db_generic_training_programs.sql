-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: smartfit_db
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `generic_training_programs`
--

DROP TABLE IF EXISTS `generic_training_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `generic_training_programs` (
  `program_id` int NOT NULL AUTO_INCREMENT,
  `machine_id` int NOT NULL,
  `category_id` int NOT NULL,
  `age_range_start` int NOT NULL,
  `age_range_end` int NOT NULL,
  `goal_type` int NOT NULL,
  `set_num` int NOT NULL,
  `repetition` int NOT NULL,
  `weight` int DEFAULT NULL,
  `rest_mins` int DEFAULT NULL,
  `comments` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`program_id`),
  KEY `fk_programs_machines_idx` (`machine_id`),
  CONSTRAINT `fk_programs_machines` FOREIGN KEY (`machine_id`) REFERENCES `machines` (`machine_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `generic_training_programs`
--

LOCK TABLES `generic_training_programs` WRITE;
/*!40000 ALTER TABLE `generic_training_programs` DISABLE KEYS */;
INSERT INTO `generic_training_programs` VALUES (1,9,3,16,45,0,4,10,20,1,'','רגליים - סקוואט עם מוט וגומייה מסביב לברכיים'),(2,1,3,16,60,0,4,10,NULL,1,'ניתן לשים גומייה','רגליים - הרמת אגן עם מוט'),(3,9,3,16,50,0,4,10,NULL,1,'ניתן לשים גומייה','ישבן - סקוואט'),(4,9,3,16,45,0,5,8,20,1,'ניתן לשים גומייה','רגליים - הרמות אגן עם מוט'),(5,4,1,13,70,0,3,15,15,2,NULL,'משקולות - הרמה מלאה'),(6,4,1,17,35,0,4,10,20,2,'ניתן להחליף ב15 אם קשה','דד ליפט משקולת אחת'),(7,10,1,14,80,0,5,4,NULL,0,'ניתן לעשות בקבוצה','ספינינג - 15 קמ\"ש'),(8,7,2,18,40,0,3,15,15,1,'ניתן לקפל את הרגליים','הרמת משקולת על כסא כומר'),(9,7,2,20,35,0,3,15,15,1,NULL,'הרמת מוט בישיבה'),(10,8,2,16,50,0,4,20,15,2,'ניתן להחליף משקולת באמצע','קייבל קרוס ידיים קדימה');
/*!40000 ALTER TABLE `generic_training_programs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-11 18:33:17
