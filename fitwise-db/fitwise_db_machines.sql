-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: smartfit_db
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `machines`
--

DROP TABLE IF EXISTS `machines`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `machines` (
  `machine_id` int NOT NULL AUTO_INCREMENT,
  `category_id` int NOT NULL,
  `machine_name` varchar(45) NOT NULL,
  `available_time_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `available_start_hour` time NOT NULL,
  `available_end_hour` time NOT NULL,
  `open_ind` tinyint DEFAULT '1',
  `if_class` tinyint DEFAULT '0',
  PRIMARY KEY (`machine_id`,`category_id`),
  UNIQUE KEY `machine_id_UNIQUE` (`machine_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `machines`
--

LOCK TABLES `machines` WRITE;
/*!40000 ALTER TABLE `machines` DISABLE KEYS */;
INSERT INTO `machines` VALUES (1,3,'ספסל','2022-09-17 12:38:51','00:00:12','00:00:20',1,0),(2,3,'הליכון','2022-09-17 12:38:51','00:00:14','00:00:23',1,0),(3,3,'סטפר','2022-09-17 12:38:51','00:00:07','00:00:19',1,0),(4,1,'משקולת','2022-09-17 12:38:51','00:00:07','00:00:23',1,0),(5,2,'פרפר','2022-09-17 12:38:51','00:00:07','00:00:16',1,0),(6,3,'פולי תחתון','2022-09-17 12:38:51','00:00:07','00:00:19',1,0),(7,2,'כיסא כומר','2022-09-17 12:38:51','00:00:12','00:00:23',1,0),(8,2,'קייבל קרוס','2022-09-17 12:38:51','00:00:16','00:00:23',1,0),(9,3,'כלוב','2022-09-17 12:38:51','00:00:10','00:00:22',1,0),(10,1,'אופניים','2022-09-20 17:09:47','00:10:00','00:23:00',1,0);
/*!40000 ALTER TABLE `machines` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-01-11 18:33:17
